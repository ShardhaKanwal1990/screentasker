//
//  Links.swift
//  ScreenTasker
//
//  Created by Mac Book on 25/09/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Foundation

enum Links: String{
    case NoAccount = "http://portal.screentasker.com/signup"
    case ForgotPassword = "http://portal.screentasker.com/forgot"
    case Dashboard = "http://portal.screentasker.com/"
}
