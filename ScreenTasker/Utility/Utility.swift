//
//  Utility.swift
//  ScreenTasker
//
//  Created by Mac Book on 23/08/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Foundation
import Cocoa

//MARK:- AppHelperUtility setup
@objc class Validation: NSObject{
    let shared = Validation()
    fileprivate override init() {}

    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    static func validateStringLength(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
}
//// MARK: AppHelperUtility setup
@objc class Utility: NSObject{
    static let main = Utility()
    fileprivate override init() {}
    
    func openURL(urlString: String){
        guard let url = URL(string: urlString) else {return}
        if NSWorkspace.shared.open(url) {
            print("default browser was successfully opened")
        }
    }
    func getAccessToken()->String?{
           return UserDefaults.standard.string(forKey: "AccessToken")
       }
    func setAccessToken(accessToken: String){
        let defaults = UserDefaults.standard
        defaults.set(accessToken, forKey: "AccessToken")
        defaults.synchronize()
    }
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    func dateFormatter(date: Date,dateFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    func hoursBetween(start: Date, end: Date) -> Int {
       Calendar.current.dateComponents([.second], from: start, to: end).second ?? 0
    }
    func dataSize(items: [Data]) -> Double{
        var MBsCount:Double = 0.0
        //        let bcf = ByteCountFormatter()
        //        bcf.allowedUnits = [.useMB]
        //        bcf.countStyle = .file
        for item in items{
            MBsCount += Double(item.count)
        }
        let MBs = (MBsCount/1024.0/1024.0)
        return Double(round(100*MBs)/100)
    }
    func secondsToTimerString(seconds: Int)-> String{
        if seconds >= 0{
            let (_, m, s) = self.secondsToHoursMinutesSeconds(seconds: seconds)
            let min = m > 9 ? "\(m)" : "0\(m)"
            let sec = s > 9 ? "\(s)" : "0\(s)"
            return "\(min):\(sec)"
            
        }
        return "00:00"
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
struct Constants {
//MARK:- Base URL
    static let BaseURL = "http://screentasker.com/api/"
    static let ServerDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static var WebCamSnap: NSImage?
    static let promodoraTime = 10
}
