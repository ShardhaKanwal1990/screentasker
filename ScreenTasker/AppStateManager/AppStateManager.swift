//
//  AppStateManager.swift
//  ScreenTasker
//
//  Created by Mac Book on 23/08/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Foundation

class AppStateManager: NSObject {
    
    static let sharedInstance = AppStateManager()
    var loggedInUser: UserModel!
    
    override init() {
        super.init()
        
        if let savedPerson = UserDefaults().object(forKey: "SavedPerson") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(UserModel.self, from: savedPerson) {
                self.loggedInUser = loadedPerson
            }
        }
        
    }
    
    func isUserLoggedIn() -> Bool{
        if let savedPerson = UserDefaults().object(forKey: "SavedPerson") as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(UserModel.self, from: savedPerson) {
                self.loggedInUser = loadedPerson
            }
        }
        
        if (self.loggedInUser) != nil {
            return true
        }
        return false
    }
    
    func loginUser(user: UserModel){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: "SavedPerson")
        }
    }
    
    func logoutUser(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "SavedPerson")
        self.loggedInUser = nil
    }
}
