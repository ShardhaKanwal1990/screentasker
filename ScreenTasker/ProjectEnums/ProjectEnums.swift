//
//  ProjectEnums.swift
//  ScreenTasker
//
//  Created by Mac Book on 24/08/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Foundation

enum SessionState {
    case Initial
    case Started
    case Paused
}
