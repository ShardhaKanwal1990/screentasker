/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Options : Codable {
	let isActivityLevelTracking : Bool?
	let iD : Int?
	let isWebcamCapture : Bool?
	let isScreenshotCapture : Bool?
	let isWindowNameTracking : Bool?
	let timeFrequencyMinutes : Int?
	let screenFrequencyMinutes : Int?
	let isTimeTracking : Bool?

	enum CodingKeys: String, CodingKey {

		case isActivityLevelTracking = "IsActivityLevelTracking"
		case iD = "ID"
		case isWebcamCapture = "IsWebcamCapture"
		case isScreenshotCapture = "IsScreenshotCapture"
		case isWindowNameTracking = "IsWindowNameTracking"
		case timeFrequencyMinutes = "TimeFrequencyMinutes"
		case screenFrequencyMinutes = "ScreenFrequencyMinutes"
		case isTimeTracking = "IsTimeTracking"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		isActivityLevelTracking = try values.decodeIfPresent(Bool.self, forKey: .isActivityLevelTracking)
		iD = try values.decodeIfPresent(Int.self, forKey: .iD)
		isWebcamCapture = try values.decodeIfPresent(Bool.self, forKey: .isWebcamCapture)
		isScreenshotCapture = try values.decodeIfPresent(Bool.self, forKey: .isScreenshotCapture)
		isWindowNameTracking = try values.decodeIfPresent(Bool.self, forKey: .isWindowNameTracking)
		timeFrequencyMinutes = try values.decodeIfPresent(Int.self, forKey: .timeFrequencyMinutes)
		screenFrequencyMinutes = try values.decodeIfPresent(Int.self, forKey: .screenFrequencyMinutes)
		isTimeTracking = try values.decodeIfPresent(Bool.self, forKey: .isTimeTracking)
	}

}