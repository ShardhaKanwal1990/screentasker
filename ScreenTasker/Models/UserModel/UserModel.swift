/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserModel : Codable {
	let options : Options?
	let emailAddress : String?
	let iD : Int?
	let departmentID : Int?
	let roleID : Int?
	let firstName : String?
	let roleName : String?
	let departmentName : String?
	let isEmployee : Bool?
	let employerAccountID : Int?
	let isPaidUpgrade : Bool?
	let lastName : String?

	enum CodingKeys: String, CodingKey {

		case options = "Options"
		case emailAddress = "EmailAddress"
		case iD = "ID"
		case departmentID = "DepartmentID"
		case roleID = "RoleID"
		case firstName = "FirstName"
		case roleName = "RoleName"
		case departmentName = "DepartmentName"
		case isEmployee = "IsEmployee"
		case employerAccountID = "EmployerAccountID"
		case isPaidUpgrade = "IsPaidUpgrade"
		case lastName = "LastName"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		options = try values.decodeIfPresent(Options.self, forKey: .options)
		emailAddress = try values.decodeIfPresent(String.self, forKey: .emailAddress)
		iD = try values.decodeIfPresent(Int.self, forKey: .iD)
		departmentID = try values.decodeIfPresent(Int.self, forKey: .departmentID)
		roleID = try values.decodeIfPresent(Int.self, forKey: .roleID)
		firstName = try values.decodeIfPresent(String.self, forKey: .firstName)
		roleName = try values.decodeIfPresent(String.self, forKey: .roleName)
		departmentName = try values.decodeIfPresent(String.self, forKey: .departmentName)
		isEmployee = try values.decodeIfPresent(Bool.self, forKey: .isEmployee)
		employerAccountID = try values.decodeIfPresent(Int.self, forKey: .employerAccountID)
		isPaidUpgrade = try values.decodeIfPresent(Bool.self, forKey: .isPaidUpgrade)
		lastName = try values.decodeIfPresent(String.self, forKey: .lastName)
	}

}
