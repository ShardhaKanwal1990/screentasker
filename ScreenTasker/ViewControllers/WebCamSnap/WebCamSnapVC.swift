//
//  WebCamSnapVC.swift
//  ScreenTasker
//
//  Created by Mac Book on 29/09/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Cocoa

class WebCamSnapVC: NSViewController {

    @IBOutlet weak var imgWebCamSnap: NSImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgWebCamSnap.image = Constants.WebCamSnap
    }
}
