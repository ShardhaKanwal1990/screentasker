//
//  WebCamSnap.swift
//  ScreenTasker
//
//  Created by Mac Book on 29/09/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Cocoa

class WebCamSnap: NSWindowController {
    
    override func windowDidLoad() {
        super.windowDidLoad()
        
        self.showWebCamSnap()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.window?.close()
        }
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

    private func showWebCamSnap(){
        let storyboard:NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("WebCamSnapVC"))
                as! WebCamSnapVC
        self.window?.contentViewController = controller
    }
}
