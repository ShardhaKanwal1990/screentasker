//
//  ViewController.swift
//  ScreenTasker
//
//  Created by Mac Book on 22/08/2020.
//  Copyright © 2020 Al-Tafseer. All rights reserved.
//

import Cocoa
import ScreenCapture
import UserNotifications

class Login: NSViewController, NSComboBoxDelegate, NSComboBoxDataSource {
    
    var timer = Timer()

    var blockTime = 0
    
    @IBOutlet weak var tfEmail: NSTextField!
    @IBOutlet weak var tfPassword: NSSecureTextField!
    @IBOutlet weak var lblValidateText: NSTextField!
    @IBOutlet weak var btnDontHaveAnAccount: NSButton!
    @IBOutlet weak var viewSessionActive: NSView!
    @IBOutlet weak var comboBoxLoggedOut: NSComboBox!
    @IBOutlet weak var comboBoxLoggedIn: NSComboBox!
    @IBOutlet weak var imgSessionState: NSImageView!
    @IBOutlet weak var btnSessionState: NSButton!
    @IBOutlet weak var imgPromodoraTimer: NSImageView!
    @IBOutlet weak var lblPromodoraTimer: NSTextField!
    @IBOutlet weak var btnPromodora: NSButton!
    @IBOutlet weak var viewLoader: NSView!
    @IBOutlet weak var piLoader: NSProgressIndicator!
    @IBOutlet weak var comboBoxProjects: NSComboBox!
    @IBOutlet weak var lblNoProjects: NSTextField!
    @IBOutlet weak var lblTimer    : NSTextField!

    var sessionState = SessionState.Initial
    var startDate: Date?
    var endDate: Date?
    var startID: Int?
    var screenshotFrequency = 120.0
    var arrProjects = [[String:Any]]()
    var selectedProject: [String:Any]?
    var webcam: Webcam?
    var lastRateCounter = Date()
    var rate = 0
    weak var timerRate: Timer?
    weak var promodoraTimer: Timer?
    var timestamp: String?
    var isPromodora = false
    var defaultPromodora = Constants.promodoraTime
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isAuthorized()
        self.setProjectsUIAsPerProjects()
//        let timer = AppController()
//        timer.showTimerWindow()
//        timer.showWindow(self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnForgotPassword(_ sender: NSButton) {
        Utility.main.openURL(urlString: Links.ForgotPassword.rawValue)
    }
    @IBAction func onBtnLogin(_ sender: NSButton) {
        self.login()
    }
    @IBAction func onBtnDontHaveAnAccount(_ sender: NSButton) {
        Utility.main.openURL(urlString: Links.NoAccount.rawValue)
    }
    @IBAction func onBtnMinimize(_ sender: NSButton) {
        NSApplication.shared.mainWindow?.miniaturize(self)
    }
    @IBAction func onBtnMore(_ sender: NSButton) {
        self.comboBoxLoggedOut.isHidden = false
        self.comboBoxLoggedOut.cell?.setAccessibilityExpanded(true)
    }
    @IBAction func onBtnMoreLoggedIn(_ sender: NSButton) {
        self.comboBoxLoggedIn.isHidden = false
        self.comboBoxLoggedIn.cell?.setAccessibilityExpanded(true)
    }
    @IBAction func onComboBoxLoggedIn(_ sender: NSComboBox) {
        if AppStateManager.sharedInstance.isUserLoggedIn(){
            guard let selectedItem = self.comboBoxLoggedIn.objectValueOfSelectedItem as? String else {return}
            switch selectedItem{
            case "Dashboard":
                Utility.main.openURL(urlString: Links.Dashboard.rawValue)
            case "Logout":
                self.stopTracking(canExit: false)
                Utility.main.setAccessToken(accessToken: "")
                self.isAuthorized()
            case "Exit":
                self.stopTracking(canExit: true)
            default:
                break
            }
        }
        else{
            guard let selectedItem = self.comboBoxLoggedOut.objectValueOfSelectedItem as? String else {return}
            switch selectedItem{
            case "Hide":
                NSApplication.shared.mainWindow?.miniaturize(self)
            case "Exit":
                exit(0)
            default:
                break
            }
        }
    }
    @IBAction func onBtnSessionState(_ sender: NSButton) {
        if self.isPromodora && self.sessionState == .Started && self.defaultPromodora > 0{
            return
        }
        switch self.sessionState {
        case .Initial:
            self.startDate = Date()
            self.sessionState = .Started
            
            self.calculateRate()
            self.rate = 10
            
            self.timestamp = "\(Date().currentTimeMillis())"
            
            self.uploadScreenShot()
//            self.uploadWebcamSnap()
            
            self.startTracking()
            self.comboBoxProjects.isEnabled = false
            
            self.setPromodoraUI()
            self.startPromodoraTimer()
        case .Started:
            self.endDate = Date()
            self.sessionState = .Paused
            
            self.rate = 0
            
            self.stopTracking(canExit: false)
            self.comboBoxProjects.isEnabled = true
            let appcontrol = AppController()
            appcontrol.killBlockerClicked()
//
//            let appDele = AppDelegate
//            appDele.
            self.setPromodoraUI()
        case .Paused:
            self.sessionState = .Started
            self.comboBoxProjects.isEnabled = false
            
            self.calculateRate()
            self.rate = 10
            
            self.timestamp = "\(Date().currentTimeMillis())"
            
            self.uploadScreenShot()
//            self.uploadWebcamSnap()
            
            self.startTracking()
            
            self.setPromodoraUI()
            self.startPromodoraTimer()
        }
        self.setSessionStateUI()
    }
    
    func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }

//    timerLabel.text = timeString(time: TimeInterval(DurationInSeconds))
    @IBAction func onTfPassword(_ sender: NSSecureTextField) {
        self.login()
    }
    @IBAction func onComboBoxProjects(_ sender: NSComboBox) {
        if self.arrProjects.isEmpty {return}
        self.selectedProject = self.arrProjects[sender.indexOfSelectedItem]
//        self.setCameraStatus()
        if let Options = self.selectedProject?["Options"] as? [String:Any]{
            if let ScreenFrequencyMinutes = Options["ScreenFrequencyMinutes"] as? Int {
                self.screenshotFrequency = Double(ScreenFrequencyMinutes) * 60.0
            }
        }
    }
    @IBAction func onBtnPromodora(_ sender: NSButton) {
        if self.sessionState == .Started{return}
        self.isPromodora = !self.isPromodora
        switch self.isPromodora{
        case true:
            sender.image = NSImage(named: NSImage.Name.init("PromodoraOff"))
        case false:
            sender.image = NSImage(named: NSImage.Name.init("PromodoraOn"))
        }
    }
    
    //MARK:- NSComboBoxDelegate
    func comboBoxWillDismiss(_ notification: Notification) {
        self.comboBoxLoggedIn.isHidden = true
        self.comboBoxLoggedOut.isHidden = true
    }
    
    //MARK:- NSComboBoxDatasource
    func numberOfItems(in comboBox: NSComboBox) -> Int {
        return self.arrProjects.count
    }
    func comboBox(_ comboBox: NSComboBox, objectValueForItemAt index: Int) -> Any? {
        return self.arrProjects[index]["ProjectName"]
    }
    
    //MARK:- Helper Methods
    
    private func validate()->[String:Any]?{
        let EmailAddress = self.tfEmail.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        let Password = self.tfPassword.stringValue
        
        if !Validation.isValidEmail(EmailAddress){
            self.lblValidateText.isHidden = false
            return nil
        }
        if !Validation.validateStringLength(Password){
            self.lblValidateText.isHidden = false
            return nil
        }
        
        self.lblValidateText.isHidden = true
        
        let params:[String:Any] = ["EmailAddress":EmailAddress,
                                   "Password":Password]
        return params
    }
    private func setSessionStateUI(){
        switch self.sessionState {
        case .Initial:
            self.imgSessionState.image = NSImage(named: "start")
//            self.lblSessionState.stringValue = "Status: Start"
        case .Started:
            self.imgSessionState.image = NSImage(named: "started")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.imgSessionState.image = NSImage(named: "paused")
//                self.lblSessionState.stringValue = "Status: Running"
            }
            
            self.postScreenShots()
        case .Paused:
            self.imgSessionState.image = NSImage(named: "start")
//            self.lblSessionState.stringValue = "Status: Start"
        }
    }
    
    private func setPromodoraUI(){
        if self.isPromodora && self.defaultPromodora > 0{
            self.imgPromodoraTimer.isHidden = false
            self.lblPromodoraTimer.isHidden = false
        }
        else{
            self.imgPromodoraTimer.isHidden = true
            self.lblPromodoraTimer.isHidden = true
        }
    }
    
    private func setProjectsUIAsPerProjects(){
        DispatchQueue.main.async {
            switch self.arrProjects.isEmpty{
            case true:
                self.imgSessionState.isHidden = true
                self.btnSessionState.isHidden = true
//                self.lblSessionState.isHidden = true
                self.lblNoProjects.isHidden = false
            case false:
                self.imgSessionState.isHidden = false
                self.btnSessionState.isHidden = false
//                self.lblSessionState.isHidden = false
                self.lblNoProjects.isHidden = true
            }
        }
    }
    private func setCameraStatus(){
        if !self.isWebcamCapture() {
            self.webcam?.stop()
            return
        }
        do {
            self.webcam = try Webcam()
        }
        catch {
            print("Webcam not initialized")
        }
        if self.webcam == nil {return}
        
        self.webcam?.start()
    }
    
    private func postScreenShots(){
        if self.sessionState == .Started{
            DispatchQueue.main.asyncAfter(deadline: .now() + self.screenshotFrequency) {
                self.timestamp = "\(Date().currentTimeMillis())"
                
                self.uploadScreenShot()
//                self.uploadWebcamSnap()
                self.postScreenShots()
            }
        }
    }
    
    func createTimer(minutes : Int) {
//        var bookingDate =  bookingModal?.start_date_time!
//        //remove the useless string from date . i.e  2018-08-04T21:00:00+00:00 after removing  2018-08-04T21:00:00
//        if let dotRange = bookingDate?.range(of: ".") {
//            bookingDate!.removeSubrange(dotRange.lowerBound..<bookingDate!.endIndex)
//        }
//        let startTime = getDateTimeForHoursInTheFuture(minutes: (Singleton.sharedInstance.ConfigurationDetails?.normal_customer_pay_time)!)
//        seconds = Int(Date().timeIntervalSince(startTime))
//        print(seconds)
        self.lblTimer.stringValue = self.timeString(time: TimeInterval(minutes))
        self.lblTimer.isHidden = false
      if timer == nil {
        let timer = Timer(timeInterval: 1.0,
          target: self,
          selector: #selector(updateTime),
          userInfo: nil,
          repeats: true)
        RunLoop.current.add(timer, forMode: .common)
        timer.tolerance = 0.1
        self.timer = timer
      }
    }
    
    @objc func updateTime() {
        blockTime = blockTime - 1
        let hours = Int(blockTime) / 3600
        let minutes = Int(blockTime) / 60 % 60
        let seconds2 = Int(blockTime) % 60
        
        var times: [String] = []
        if hours > -1 {
          times.append("\(hours)")
        }
        if minutes > -1 {
          times.append(": \(minutes)")
        }
        if blockTime < 0 {
            lblTimer.stringValue = "00 : 00 : 00"
            lblTimer.isHidden = true
            let appcont = AppController()
            appcont.killBlockerClicked()
            timer.invalidate()
        }
        times.append(" : \(seconds2)")
//        lblTimer.isHidden = false

        lblTimer.stringValue = times.joined(separator: " ")
    }
    
    func getTimeDiffrence(startTime: Date, endTime:Date) -> Int {
        let differenceInSeconds = Int(endTime.timeIntervalSince(startTime))
        print(differenceInSeconds)
        return differenceInSeconds
    }
    
    private func getImageQualityWithLevel(image: NSImage, level: CGFloat) -> NSImage {
        let _image = image
        
        
        let newRect: NSRect = NSMakeRect(0, 0, CGFloat(image.CGImage?.width ?? 0), CGFloat(image.CGImage?.height ?? 0))
        
        let imageSizeH: CGFloat = _image.size.height * level
        let imageSizeW: CGFloat = _image.size.width * level
        
        
        let newImage = NSImage(size: NSMakeSize(imageSizeW, imageSizeH))
        newImage.lockFocus()
        NSGraphicsContext.current?.imageInterpolation = NSImageInterpolation.low
        
        
        _image.draw(in: NSMakeRect(0, 0, imageSizeW, imageSizeH), from: newRect, operation: NSCompositingOperation.screen, fraction: 1)
        newImage.unlockFocus()
        
        return newImage
    }
    private func getScreenShot()->Data?{
        let imgPath: String = ScreenCapture.captureScreen("\(Utility.main.randomString(length: 16)).png").path
        guard let screenShot: NSImage = NSImage(contentsOfFile: imgPath) else {return nil}
        let compressedScreenShot = self.getImageQualityWithLevel(image: screenShot, level: 0.6)
        guard let data = compressedScreenShot.tiffRepresentation else {return nil}
        return data
    }
    private func getLastActiveWindowTitle()->String?{
       //find all the windows (CGWindows)
        let options = CGWindowListOption(arrayLiteral: CGWindowListOption.optionOnScreenAboveWindow)
        let cgWindowListInfo = CGWindowListCopyWindowInfo(options, CGWindowID(0))
        let cgWindowListInfo2 = cgWindowListInfo as NSArray? as? [[String: AnyObject]]

        //find the active apps process id
        let frontMostAppID = Int(NSWorkspace.shared.frontmostApplication!.processIdentifier)

        for windowDic in cgWindowListInfo2! {
            let windowOwner = windowDic["kCGWindowOwnerName"] as? String ?? ""
            let windowTitle = (windowDic["kCGWindowName"] as? String ?? "").isEmpty ? "Loading" : (windowDic["kCGWindowName"] as? String ?? "")
            let title = "\(windowOwner): \(windowTitle)"
            
            // find ative windows
            let ownerProcessID = windowDic["kCGWindowOwnerPID"] as! Int
            if frontMostAppID == ownerProcessID {
                return title
            }
        }
        return nil
    }
    private func getWebcamSnap(image: NSImage)->Data?{
        let compressedScreenShot = self.getImageQualityWithLevel(image: image, level: 0.6)
        guard let data = compressedScreenShot.tiffRepresentation else {return nil}
        return data
    }
    private func getMacOSIdleTime()->Int{
        var lastEvent:CFTimeInterval = 0
        lastEvent = CGEventSource.secondsSinceLastEventType(CGEventSourceStateID.hidSystemState, eventType: CGEventType(rawValue: ~0)!)
        return Int(lastEvent)
    }
    
    private func showLoader(){
        self.tfEmail.isEditable = false
        self.tfPassword.isEditable = false
        self.piLoader.startAnimation(self)
        self.viewLoader.isHidden = false
    }
    private func hideLoader(){
        DispatchQueue.main.async {
            self.tfEmail.isEditable = true
            self.tfPassword.isEditable = true
            self.piLoader.stopAnimation(self)
            self.viewLoader.isHidden = true
        }
    }
    
    private func isActivityLevelTracking()->Bool{
        if let Options = self.selectedProject?["Options"] as? [String:Any]{
            if let optionStatus = Options["IsActivityLevelTracking"] as? Bool {
                return optionStatus
            }
        }
        return false
    }
    private func isScreenshotCapture()->Bool{
        if let Options = self.selectedProject?["Options"] as? [String:Any]{
            if let optionStatus = Options["IsScreenshotCapture"] as? Bool {
                return optionStatus
            }
        }
        return false
    }
    private func isTimeTracking()->Bool{
        if let Options = self.selectedProject?["Options"] as? [String:Any]{
            if let optionStatus = Options["IsTimeTracking"] as? Bool {
                return optionStatus
            }
        }
        return false
    }
    private func isWebcamCapture()->Bool{
        if let Options = self.selectedProject?["Options"] as? [String:Any]{
            if let optionStatus = Options["IsWebcamCapture"] as? Bool {
                return optionStatus
            }
        }
        return false
    }
    private func isWindowNameTracking()->Bool{
        if let Options = self.selectedProject?["Options"] as? [String:Any]{
            if let optionStatus = Options["IsWindowNameTracking"] as? Bool {
                return optionStatus
            }
        }
        return false
    }
     
    private func calculateRate(){
        self.rate = 0;
        self.lastRateCounter = Date()
        
        self.timerRate = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.screenshotFrequency/10.0), repeats: true) { [weak self] _ in
            let calendar = Calendar.current
            let lastActive = calendar.date(byAdding: .second, value: -(self?.getMacOSIdleTime() ?? 0), to: Date()) ?? Date()
            
            let lastRateCounter = self?.lastRateCounter ?? Date()
            
            if (lastActive >= lastRateCounter) {
                self?.rate += 1
                print("not idle")
                print(self?.getLastActiveWindowTitle() ?? "None")
            }
            else{
                print("idle")
                print(self?.getLastActiveWindowTitle() ?? "None")
            }
            if (self?.rate ?? 0) > 10 {
                self?.rate = 10
            }
            
            self?.lastRateCounter = Date()
        }
        
    }
    
    private func startPromodoraTimer(){
        
        self.promodoraTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(1), repeats: true) { [weak self] _ in
            guard let isPromodora = self?.isPromodora, isPromodora == true else {return}
            if self?.defaultPromodora == 0{
                //play gif
                self?.promodoraTimer?.invalidate()
                self?.sessionState = .Initial
                self?.setSessionStateUI()
                self?.setPromodoraUI()
                self?.defaultPromodora = Constants.promodoraTime
                return
            }
            self?.defaultPromodora -= 1
            self?.lblPromodoraTimer.stringValue = Utility.main.secondsToTimerString(seconds: self?.defaultPromodora ?? 0)
        }
        
    }
    
    private func showWebCamSnap(){
        let storyboard:NSStoryboard = NSStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateController(withIdentifier: "WebCamSnap") as? WebCamSnap else {return}
        controller.showWindow(self)
    }
    
    //MARK:- Services
    private func login(){
        self.showLoader()
        guard let params = self.validate() else {return}
        
        guard let url = URL(string: Constants.BaseURL+"Account/Login") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            self.hideLoader()
            do {
                
                if let accessToken = (response as? HTTPURLResponse)?.allHeaderFields["Token"] as? String{
                    Utility.main.setAccessToken(accessToken: accessToken)
                }
                
                guard let data = data else {
                    DispatchQueue.main.async {
                        self.lblValidateText.isHidden = false
                        self.lblValidateText.stringValue = error?.localizedDescription ?? "Unable login at this time"
                    }
                    return
                }
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode(LoginModel.self, from: data)
                guard let status = responseModel.response else {return}
                switch status{
                case true:
                    DispatchQueue.main.async {
                        self.lblValidateText.isHidden = true
                        AppStateManager.sharedInstance.loginUser(user: responseModel.result!)
                        self.isAuthorized()
                    }
                case false:
                    DispatchQueue.main.async {
                        self.lblValidateText.isHidden = false
                        self.lblValidateText.stringValue = responseModel.message ?? "Invalid username or password."
                    }
                }
            } catch {
                print("error")
            }
        })
        task.resume()
    }
    private func isAuthorized(){
        self.showLoader()
        guard let url = URL(string: Constants.BaseURL+"Account/IsAuthrized") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.main.getAccessToken() ?? "", forHTTPHeaderField:"Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            self.hideLoader()
            if let statusCode = (response as? HTTPURLResponse)?.statusCode{
                if statusCode == 200{
                    DispatchQueue.main.async {
                        self.viewSessionActive.isHidden = false
                        self.tfEmail.isEditable = false
                        self.tfPassword.isEditable = false
                        self.btnDontHaveAnAccount.isEnabled = false
                        self.getAllProjects()
                    }
                }
                else{
                    DispatchQueue.main.async {
                        Utility.main.setAccessToken(accessToken: "")
                        self.viewSessionActive.isHidden = true
                        AppStateManager.sharedInstance.logoutUser()
                        self.tfEmail.isEditable = true
                        self.tfPassword.isEditable = true
                        
                        self.tfEmail.stringValue = ""
                        self.tfPassword.stringValue = ""
                        
                        self.tfEmail.becomeFirstResponder()
                        
                        self.btnDontHaveAnAccount.isEnabled = true
                        
                        self.sessionState = SessionState.Initial
                        self.setSessionStateUI()
                    }
                }
            }
        })
        task.resume()
    }
    
    private func createRequest(data: Data, IsWebcam:Bool) throws -> URLRequest {
        
        let boundary = generateBoundaryString()
        let url = URL(string: Constants.BaseURL+"Tracking/Screen")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.main.getAccessToken() ?? "", forHTTPHeaderField:"Authorization")
        request.setValue("mac", forHTTPHeaderField:"platform")
        
        let ProjectID = "\(self.selectedProject?["ID"] as? Int ?? 0)"
        let ActivityRate = "\(self.rate)"
        let WindowName = self.getLastActiveWindowTitle() ?? ""
        
        let params:[String:Any] = ["ProjectID":ProjectID,
                                   "ActivityRate":ActivityRate,
                                   "WindowName":WindowName,
                                   "IsWebcam":IsWebcam]
        print(params)
        
        request.httpBody = try createBody(with: params, filePathKey: "Screen", urls: [], boundary: boundary, data: data)
        
        return request
    }
    private func createBody(with parameters: [String: Any]?, filePathKey: String, urls: [URL], boundary: String, data: Data) throws -> Data {
        var body = Data()
        
        if parameters != nil{
            for(key, value) in parameters!{
                // Add the reqtype field and its value to the raw http request data
                body.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                body.append("\(value)".data(using: .utf8)!)
            }
        }
        
        let filename = (self.timestamp ?? "") + ".png"
        let mimetype = mimeType(for: filename)
        
        body.append("\r\n--\(boundary)\r\n")
        body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
        body.append("Content-Type: \(mimetype)\r\n\r\n")
        body.append(data)
        body.append("\r\n")
        
        body.append("--\(boundary)--\r\n")
        
        return body
    }
    private func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    private func mimeType(for path: String) -> String {
        let pathExtension = URL(fileURLWithPath: path).pathExtension as NSString
        
        guard
            let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension, nil)?.takeRetainedValue(),
            let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue()
            else {
                return "application/octet-stream"
        }
        
        return mimetype as String
    }
    
    private func uploadScreenShot(){
        if !self.isScreenshotCapture() {return}
        let screenShotData = self.getScreenShot() ?? Data()
        let request = try? self.createRequest(data: screenShotData, IsWebcam: false)
        let data = request?.httpBody ?? Data()
        let session = URLSession.shared
        session.uploadTask(with: request!, from: data, completionHandler: { responseData, response, error in
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                    self.rate = 0
                }
            }
        }).resume()
    }
    private func uploadWebcamSnap(){
        guard let webcam = self.webcam else { return }
        webcam.captureImage(result: { result in
            switch result {
            case .image(let image):
                print(image)
                Constants.WebCamSnap = image
                self.showWebCamSnap()
                let screenShotData = self.getWebcamSnap(image: image) ?? Data()
                let request = try? self.createRequest(data: screenShotData, IsWebcam: true)
                let data = request?.httpBody ?? Data()
                let session = URLSession.shared
                session.uploadTask(with: request!, from: data, completionHandler: { responseData, response, error in
                    if error == nil {
                        let jsonData = try? JSONSerialization.jsonObject(with: responseData!, options: .allowFragments)
                        if let json = jsonData as? [String: Any] {
                            print(json)
                        }
                    }
                }).resume()
            case .error(_):
                break
            }
        })
    }
    
    func getDateTimeForHoursInTheFuture(hours: Int) -> Date {
        print(Date())
        var components = DateComponents();
        components.setValue(-hours, for: .minute);
        let date: Date = Date();
        let expirationDate = Calendar.current.date(byAdding: components, to: date);
        return expirationDate!;
//        var components = DateComponents();
//        components.setValue(hours, for: .hour);
//        let date: Date = Date();
//        let expirationDate = Calendar.current.date(byAdding: components, to: date);
//
//        return expirationDate!;
    }
    
    func getDateTimeForHoursInTheFutureInSeconds(hours: Int) -> Date {
            print(Date())
            var components = DateComponents();
            components.setValue(-hours, for: .second);
            let date: Date = Date();
            let expirationDate = Calendar.current.date(byAdding: components, to: date);
            return expirationDate!;
    //        var components = DateComponents();
    //        components.setValue(hours, for: .hour);
    //        let date: Date = Date();
    //        let expirationDate = Calendar.current.date(byAdding: components, to: date);
    //
    //        return expirationDate!;
        }
    
    //MARK:- timer
    private func startTracking(){
        if !self.isTimeTracking() {return}
        let TrackTime = Utility.main.dateFormatter(date: self.startDate ?? Date(), dateFormat: Constants.ServerDateFormat)
        let ProjectID = self.selectedProject?["ID"] as? Int ?? 0
        
        let params:[String:Any] = ["TrackTime":TrackTime,
                                   "ProjectID":ProjectID]
        
        guard let url = URL(string: Constants.BaseURL+"Tracking/Start") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.main.getAccessToken() ?? "", forHTTPHeaderField:"Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                    guard let response = json["Result"] as? [String: Any] else {return}
                    guard let ID = response["ID"] as? Int else {return}
                    self.startID = ID
                print(self.getDateTimeForHoursInTheFuture(hours: 1))
//                    NSLog("%i", self.getDateTimeForHoursInTheFuture(hours: 1))
                    let time = self.getDateTimeForHoursInTheFuture(hours: 1)
                    let minutes = Int(Date().timeIntervalSince(time))
                    let timeInSeconds = self.getDateTimeForHoursInTheFutureInSeconds(hours: 3600)
//                    let convertedSeconds = Int(Date().timeIntervalSince(timeInSeconds))

                    let appControll = AppController()
                    appControll.updateTimeSliderDisplayUpdate(Int32(minutes))
                    appControll.addToBlock()
                    self.blockTime = minutes
                    self.createTimer(minutes: minutes)
                    
//                    self.lblTimer.stringValue = self.timeString(time: TimeInterval(minutes))
//                    self.lblTimer.isHidden = false
//                    let timer = TimerWindowController()
//                    timer.awakeFromNib()
//                    timer.blockEndDateUpdated(self.getDateTimeForHoursInTheFuture(hours: 1))
//                    timer.blockEndDateUpdated
//                    timer.
                }
            }
        })
        task.resume()
    }
    private func stopTracking(canExit:Bool){
        if !self.isTimeTracking() {return}
        let StartID = self.startID ?? 0
        let TrackTime = Utility.main.dateFormatter(date: self.endDate ?? Date(), dateFormat: Constants.ServerDateFormat)
        let params:[String:Any] = ["TrackTime":TrackTime,
                                   "StartID":StartID]
        
        guard let url = URL(string: Constants.BaseURL+"Tracking/End") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        request.addValue("application/json-patch+json", forHTTPHeaderField: "Content-Type")
        request.setValue(Utility.main.getAccessToken() ?? "", forHTTPHeaderField:"Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    DispatchQueue.main.async {
                        self.comboBoxProjects.isEnabled = true
                    }
                    print(json)
                    if canExit{exit(0)}
                }
            }
        })
        task.resume()
    }
    
    private func getAllProjects(){
        guard let url = URL(string: Constants.BaseURL+"Project/AccountOfProjects") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue(Utility.main.getAccessToken() ?? "", forHTTPHeaderField:"Authorization")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                    guard let response = json["Result"] as? [[String: Any]] else {return}
                    self.arrProjects = response
                    self.setProjectsUIAsPerProjects()
                    if !self.arrProjects.isEmpty{
                        self.selectedProject = self.arrProjects.first
//                        self.setCameraStatus()
                        if let Options = self.selectedProject?["Options"] as? [String:Any]{
                            if let ScreenFrequencyMinutes = Options["ScreenFrequencyMinutes"] as? Int {
                                self.screenshotFrequency = Double(ScreenFrequencyMinutes) * 60.0
                            }
                        }
                        DispatchQueue.main.async {
                            self.comboBoxProjects.reloadData()
                            self.comboBoxProjects.selectItem(at: 0)
                        }
                    }
                    print(self.arrProjects)
                }
            }
        })
        task.resume()


    }
}
extension Data {
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
extension NSImage {
    @objc var CGImage: CGImage? {
        get {
            guard let imageData = self.tiffRepresentation else { return nil }
            guard let sourceData = CGImageSourceCreateWithData(imageData as CFData, nil) else { return nil }
            return CGImageSourceCreateImageAtIndex(sourceData, 0, nil)
        }
    }
}
extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
