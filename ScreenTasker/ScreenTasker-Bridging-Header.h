//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import "TimerWindowController.h"
#import "AppController.h"
//#import "AppDelegate.h"
/*- (void)killBlockerClicked {
    AuthorizationRef authorizationRef;
    char* helperToolPath = [self selfControlKillerHelperToolPathUTF8String];
    NSUInteger helperToolPathSize = strlen(helperToolPath);
    AuthorizationItem right = {
        kAuthorizationRightExecute,
        helperToolPathSize,
        helperToolPath,
        0
    };
    AuthorizationRights authRights = {
        1,
        &right
    };
    AuthorizationFlags myFlags = kAuthorizationFlagDefaults |
    kAuthorizationFlagExtendRights |
    kAuthorizationFlagInteractionAllowed;
    OSStatus status;

    status = AuthorizationCreate (&authRights,
                                  kAuthorizationEmptyEnvironment,
                                  myFlags,
                                  &authorizationRef);

    if(status) {
        NSLog(@"ERROR: Failed to authorize block kill.");
        return;
    }
    
    // we're about to launch a helper tool which will read settings, so make sure the ones on disk are valid
    [[SCSettings currentUserSettings] synchronizeSettings];

    char uidString[10];
    snprintf(uidString, sizeof(uidString), "%d", getuid());

    char* args[] = { uidString, NULL };

    status = AuthorizationExecuteWithPrivileges(authorizationRef,
                                                helperToolPath,
                                                kAuthorizationFlagDefaults,
                                                args,
                                                NULL);
    if(status) {
        NSLog(@"WARNING: Authorized execution of helper tool returned failure status code %d", status);

        NSError* err = [NSError errorWithDomain: @"org.eyebeam.SelfControl-Killer" code: status userInfo: @{NSLocalizedDescriptionKey: @"Error executing privileged helper tool."}];

        [NSApp presentError: err];

        return;
    } else {
        NSAlert* alert = [[NSAlert alloc] init];
        [alert setMessageText: @"Success!"];
        [alert setInformativeText:@"The block was cleared successfully.  You can find the log file, named SelfControl-Killer.log, in your Documents folder."];
        [alert addButtonWithTitle: @"OK"];
        [alert runModal];
    }

    [self.viewButton setEnabled: YES];
}
*/
